# ISO8601-ish-ish Clock

![Screenshot](media/screenshot.png)

> For **Gnome 45**

Forked from the esteemed [S410's original extension](https://extensions.gnome.org/extension/6413/iso8601-ish-clock/)

Why -ish? Because using a space instead of a T

Why -ish-ish? Because it also allows for day of the week and AM / PM display

This extension uses preferences from the Gnome Settings app

![Settings](media/screenshot2-light.png)
